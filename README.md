# Katalog

A collection of language independent coding katas, doable in 3 hours or less.
I did not invent any of these (except for Risk and TODO list) but found them online.
My goal is to gather the ones that help me perfect my craft and also teach !

## Summary

+ Tennis game (=> https://github.com/emilybache/Tennis-Refactoring-Kata)
+ Transitive dependencies (=> http://codekata.com/kata/kata18-transitive-dependencies/)
+ Roman numerals (=> http://agilekatas.co.uk/katas/RomanNumerals-Kata)
+ Mars rover (=> https://technologyconversations.com/2014/10/17/java-tutorial-through-katas-mars-rover/)
+ Banking (=> http://kata-log.rocks/banking-kata)
+ Risk - Game of Thrones (=> https://gitlab.com/blndr/risk)
+ TODO List (=> https://gitlab.com/blndr/lyst)

___

### Tennis game

Tennis has a rather quirky scoring system, and to newcomers it can be a little difficult to keep track of. The tennis society has contracted you to build a scoreboard to display the current score during tennis games.
Your task is to write a “TennisGame” class containing the logic which outputs the correct score as a string for display on the scoreboard. When a player scores a point, it triggers a method to be called on your class letting you know who scored the point. Later, you will get a call “score()” from the scoreboard asking what it should display. This method should return a string with the current score.
You can read more about Tennis scores here which is summarized below:

+ A game is won by the first player to have won at least four points in total and at least two points more than the opponent.
+ The running score of each game is described in a manner peculiar to tennis: scores from zero to three points are described as "Love", "Fifteen", "Thirty", and "Forty" respectively.
+ If at least three points have been scored by each player, and the scores are equal, the score is "Deuce".
+ If at least three points have been scored by each side and a player has one more point than his opponent, the score of the game is "Advantage" for the player in the lead.

You need only report the score for the current game. Sets and Matches are out of scope.
___

### Transitive dependencies

One of the insidious things about dependencies is that they are transitive - if A depends on B and B depends on C, then A also depends on C.
So, let’s write some code that calculates the full set of dependencies for a group of items.
The code takes as input a set of lines where the first token is the name of an item.
The remaining tokens are the names of things that this first item depends on.
Given the following input, we know that A directly depends on B and C, B depends on C and E, and so on.

```
A > B, C
B > C, E
C > G
D > A, F
E > F
F > H
```

The program should use this data to calculate the full set of dependencies.
For example, looking at B, we see it directly depends on C and E.
C in turn relies on G, E relies on F, and F relies on H.
This means that B ultimately relies on C, E, F, G, and H.
In fact, the full set of dependencies for the previous example is:

```
A > B, C, E, F, G, H
B > C, E, F, G, H
C > G
D > A, B, C, E, F, G, H
E > F, H
F > H
```
___

### Roman numerals

The Romans wrote their numbers using letters.
Secifically, the following letters were used:

+ 'I' meaning '1'
+ 'V' meaning '5'
+ 'X' meaning '10'
+ 'L' meaning '50'
+ 'C' meaning '100'
+ 'D' meaning '500'
+ 'M' meaning '1000'

There were certain rules that the numerals followed which should be observed.

+ The symbols 'I', 'X', 'C', and 'M' can be repeated at most 3 times in a row.
+ The symbols 'V', 'L', and 'D' can never be repeated.
+ The '1' symbols ('I', 'X', and 'C') can only be subtracted from the 2 next highest values ('IV' and 'IX', 'XL' and 'XC', 'CD' and 'CM').
+ Only one subtraction can be made per numeral ('XC' is allowed, 'XXC' is not).
+ The '5' symbols ('V', 'L', and 'D') can never be subtracted.

Your goal is to code a function that converts an arabic number (i.e. a base-10 integer) to a ruman numeral (i.e. a string).

### Mars rover

Develop an api that moves a rover around on a grid.
You are given the following specifications:

+ You are given the initial starting point (x,y) of a rover and the direction (N,S,E,W) it is facing.
+ The rover receives a character array of commands.
+ Implement commands that move the rover forward/backward (f,b).
+ Implement commands that turn the rover left/right (l,r).
+ Implement wrapping from one edge of the grid to another. (planets are spheres after all)
+ Implement obstacle detection before each move to a new square.
+ If a given sequence of commands encounters an obstacle, the rover moves up to the last possible point and reports the obstacle.

### Banking

Write a class Account that offers the following methods:
+ void deposit(int)
+ void withdraw(int)
+ String printStatement()

Careful : with each banking operation is associated the date of its occurence !
Also, the client is very demanding about the output of the system, a.k.a. the statement.
An example of such a statement would be:

```
Date          Amount      Balance
2015.12.24    +500        500
2016.08.23    -100        400
```

### Risk - Game of Thrones

__This kata is fully described here : https://gitlab.com/blndr/risk__

This is a simplification of a Risk game, featuring the Houses and Realms of the Game of Thrones universe - restricted to Westeros.
The battle mechanism is not the same as in the actual Risk board game though : it has been simplified but still uses 'dices' :-)
Beware : this is a long kata as it might take you more than 2 hours to finish.
I created it as a way to teach software design and test driven development.
It is recommended to have an acting Product Owner during the session, one who could answer the coders questions on eventual ambiguities in the requirements.

### TODO List

The goal of this kata is to write a TODO list management app with the constraint of following a hexagonal / clean architecture, a.k.a. organizing your code using 3 layers :
+ the adapter layer: in charge of allowing your app to use a specific environment
+ the port layer: in charge of providing interfaces for the adapters to interact with the domain
+ the domain: in charge of implementing the use cases and the domain model

Your app should allow the user to :
+ add a theme to the project
+ add an idea to the current theme
+ tag an idea as 'done'
+ switch the current theme to another one
+ display the project tree (1 project > N themes > P ideas)
+ load a project from a data source
+ save the project to a data source